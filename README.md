
## OSTraining "How to Use the Twig Template video course"

https://www.ostraining.com/class/twig

### Install Twig

```bash
$ pwd
$ <project-dir>/twig-tutorial  

# Install twig and dependencies in vendor/ 
$ composer require twig/twig  


```
### Create application files
<docroot>/app/front.php
<docroot>/views/front.twig.html

### Run application

```bash
$ php -S localhost:8000
```

### Point browser to:
localhost:8000/app/front.php
