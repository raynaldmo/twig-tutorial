<?php 
  require_once '../vendor/autoload.php'; 
  $loader = new Twig_Loader_Filesystem('../views/');
  $twig = new Twig_Environment($loader);
  
  $arrayVariable = array(
    'one' => 'pidgeons',
    'two' => 'hammy'
  );

  echo $twig->render(
    'page.html', 
    array(
      'text' => 'Twig is working.',
      'myBox' => new Box,
      'someArray' => array(
        'firstThing' => 'monsters',
        'secondThing' => 'robots'
      ),
      'factoredArray' => $arrayVariable
    )
  );

  echo $twig->render(
    'otherpage.html',
    array(
      'text2'=> 'elegant gypsy'
    )
  );

  class Box {
    public $shape = 'square';

    public function displayShape() {
      return $this->shape;
    }
  }

?>
