<?php

// Example: Object data
class Tree {
    private $label;
    private $genus;
    private $bloomingSeasons;
    public $description;

    public function __construct(string $label, string $genus, array $seasons)
    {
        $this->label = $label;
        $this->genus = $genus;
        $this->bloomingSeasons = $seasons;
    }

    public function getLabel() : string {
        return $this->label;
    }

    public function getGenus() : string {
        return $this->genus;
    }

    public function getBloomingSeasons() : string {
        return implode(', ', $this->bloomingSeasons);
    }
}