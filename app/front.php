<?php
require_once __DIR__ . '/Tree.php';

// Setup Twig
require_once '../vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('../views/');
$twig = new Twig_Environment($loader);

// Example: Array data
$siteInfo = [
  'title' => 'Tree Fans',
  'slogan' => 'This is a site for those who love trees'
];

// Instantiate data
$treesOFTheMonth = array();

$dogwood = new Tree('dogwood', 'Cornus', ['fall']);
$dogwood->description = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu';
array_push($treesOFTheMonth, $dogwood);

$cherryBlossom = new Tree('cherry blossom', 'Prunus', ['fall','winter']);
array_push($treesOFTheMonth, $cherryBlossom);

// Render page
echo $twig->render(
  // 'front.html.twig',
  'front.html.twig',
  [ 'siteInfo' => $siteInfo,
    'treesOfTheMonth' => $treesOFTheMonth,
  ]
);